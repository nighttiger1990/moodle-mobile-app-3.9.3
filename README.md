Moodle Mobile
=================

This is the primary repository of source code for the official Moodle Mobile app.

* [User documentation](http://docs.moodle.org/en/Moodle_Mobile)
* [Developer documentation](http://docs.moodle.org/dev/Moodle_Mobile)
* [Development environment setup](http://docs.moodle.org/dev/Setting_up_your_development_environment_for_Moodle_Mobile_2)
* [Bug Tracker](https://tracker.moodle.org/browse/MOBILE)
* [Release Notes](http://docs.moodle.org/dev/Moodle_Mobile_Release_Notes)

License
-------

[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0)

Big Thanks
-----------

Cross-browser Testing Platform and Open Source <3 Provided by [Sauce Labs](https://saucelabs.com)

![Sauce Labs Logo](https://user-images.githubusercontent.com/557037/43443976-d88d5a78-94a2-11e8-8915-9f06521423dd.png)

Tech required: 
* Nodejs v12.18.1
* Ionic

## Pre install
```bash
npm install -g @ionic/cli
npm i -g cordova-res
npm install
# generate icon and splash for android and ios base (./resources/icon.png and ./resources/splash.png)
ionic cordova resources
```
with IOS need need install `cocoapods`
```bash
gem install cocoapods
```

## build for Android
example keystore

`tnttalent-release-key.keystore | admin@123##`

```bash
#note remember change id in google-service.json if change id in config.xml
ionic cordova resources
ionic cordova prepare android
ionic cordova build android --prod --release --verbose
# gen key if dont have
keytool -genkey -v -keystore tntalent-release-key.keystore -alias tntalent -keyalg RSA -keysize 2048 -validity 10000
# signed to apk
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore tntalent-release-key.keystore ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk tntalent
# cd ~/Library/Android/sdk/build-tools/29.0.2/
./zipalign -v 4 /Users/macos/Desktop/Project/Ionic/tntalent/platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk /Users/macos/Desktop/Project/Ionic/tntalent/platforms/android/app/build/outputs/apk/release/tntalent-release.apk

```
## build for IOS
Need install 
```bash
#note remember change id in google-service.json if change id in config.xml
ionic cordova resources 
ionic cordova prepare ios


```
### using xcode for building development or production
```bash
open platforms/ios/TNTalent.xcworkspace 
```
Click button `Play` for build Development
For development need re-setting Provision in `Signing & Capabilities` tab

### using xcode for building production
run this for bundle js code for production mode
```bash
ionic cordova build ios --prod

# Some special build
#ionic cordova build ios --prod --release --buildFlag="-UseModernBuildSystem=0" -- --developmentTeam="SA5RXMLX9L" --codeSignIdentity="iPhone Distribution" --provisioningProfile="TNTalent Distribution" --packageType="app-store" #success
```
then using xcode and compile project

Click Menu `Product` => `Archive`
After Archive success. click `Distribute App` 

something we got problem with build development need re run pod install
```bash
cd platforms/ios && pod install
```

# Some tutorials for building production with xcode
* [How to Submit IOS](https://instabug.com/blog/how-to-submit-app-to-app-store/)